# Android development
export PATH="/Users/justeningels/Library/Android/sdk/emulator:/Users/justeningels/Library/Android/sdk/build-tools/30.0.3:/Users/justeningels/Library/Android/sdk/platform-tools:${PATH}"
# Fix DNS issue when starting emulator:
# emulator -list-avds
# emulator @{MY_EMULATOR} -dns-server 8.8.8.8

# Disable behavior of keeping history per shell session
# I think this is only for BASH, not ZSH
export SHELL_SESSION_HISTORY=0
# alias for easy history grepping
alias h='history -2000 | grep'
# ZSH config
HISTSIZE=100000000
SAVEHIST=100000000
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.

# Python environment manager pyenv and pyenv-virtualenv
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PATH="$(pyenv root)/shims:$(pyenv root)/bin:$PATH"
# Aliases for pyenv
alias py=pyenv
alias pysh='pyenv shell'
alias pygl='pyenv global'

# Intellij idea startup
export PATH="/Applications/IntelliJ IDEA CE.app/Contents/MacOS:${PATH}"

# Alias to use the native resolution of the Macbook Pro's retina screen
# alias nativeres='~/GIT/screenutil/scrutil s 2880 1800 16'
# Update: the alias above does not work anymore, instead use the software
# called 'EasyRes'

# Aliases for rsync
# You can always use a dryrun (-n) argument at the cpx aliases
# Example usage
# 1. Copy CONTENTS of dir1 to dir2 (results in dir2/<files>) 
# $ cpr dir1/ dir2/
# 2. Copy DIRECTORY dir1 to dir2 (results in dir2/dir1/<files>)
# $ cpr dir1 dir2/  # Notice absense of trailing slash
alias cpr='rsync -ah'
alias cpv='cpr -v'
alias cpp='cpv --progress'
alias cptp='cpv --total-progress'
# Two good explanations on using partial copies is found in this answer on StackExchange:
# Both are answers to the same question
# https://unix.stackexchange.com/a/165417
# https://unix.stackexchange.com/a/252969

# Git commands
# Oneliner to remove all branches if
#  - they ARE MERGED into the CURRENT BRANCH
#  - they ARE NOT dev / master
# First check with:
# $ git branch --merged | egrep -v "(^\*|master|dev)"
# Then delete with:
# $ git branch --merged | egrep -v "(^\*|master|dev)" | xargs git branch -d

# Oneliner to clean-up outdated remote branch references
# $ git remote prune origin

# macOS network troubleshooting
# To show IP routes:
# $ netstat -nr

# Autojump
[ -f /usr/local/etc/profile.d/autojump.sh ] && . /usr/local/etc/profile.d/autojump.sh

# GIT Autojump
function c {
  OUTPUT=$(git for-each-ref --sort=committerdate --format='%(refname:short)' refs/heads/)
  
  for filterarg in "$@"
  do
    OUTPUT=$(echo "$OUTPUT" | grep $filterarg)
  done

  BRANCH=$(echo "$OUTPUT" | tail -1)
  if [[ -z "$BRANCH" ]]
  then
    echo "No branch found"
  else
    echo $BRANCH  
    git checkout $BRANCH && echo -e "\e[31m$BRANCH\e[0m"
  fi
}

### Home server
# SSH
alias vm='ssh -p 3022 jusx@localhost'
# Wake on lan
alias svboot='wakeonlan a8:a1:59:83:7a:b1'
alias svoff='ssh -t server sudo poweroff'
